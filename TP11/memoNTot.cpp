using namespace std;
#include <iostream>
#include <vector>
#include <cstdlib>
#define INF 10000;

//tab val piece 
vector<int> v = {1,2,5,10};
//nb val de piece
int n = v.size();
//tab de memoisation
vector<bool> memoized;
vector<int> ntot;
//tab meilleurs choix
vector<int> bestChoices;


int memoNTot(int m)
{
    if (m<0)
    {
        return INF; 
    }
    else if (memoized[m]){
        //si sln memoisee on la renvoie directement
        return ntot[m];
    } 
    else if (m == 0)
    {
        return 0;
    }
    
    else
    {
        int min = INF;
        for (int i = 0; i < n ; i++){
            int current = memoNTot(m-v[i]) +1;
            if (current<min){
                min=current;
               // bestChoices[m] = i; segmentation fault 
            }
        }
        ntot[m] = min;
        memoized[m] = true;
        return min;
    }
}
    
int main(int argc, const char *argv[]) 
{
    if (argc == 1){
        cout << "entre une valeur sahbe" << endl;
    }
    else {
        int m = atoi(argv[1]);
        //initialisation tab memo
        memoized = vector<bool>(m+1, false);
        ntot = vector<int>(m+1,0);        
        cout << "pour rendre " << m << " euro, il faut " << memoNTot(m) << " pièces." << endl;

        //afficher les pieces 
       /* bestChoices = vector<int>();
        int i=1;
        int p=m;
        while (p>0){
            //tant que le montant a rendre est > 0 on affiche les pieces
            cout << "piece " << i++ << " : " << v[bestChoices[p]] << endl;
            //maj du montant
            p=p-v[bestChoices[p]];
        }*/
    }
    

    return 0;
}
