using namespace std;
#include <iostream>
#include <vector>
#include <cstdlib>
#define INF 10000;

//tab val piece 
vector<int> v = {1,2,5,10};
//nb val de piece
int n = v.size();

int bestNTot(int m)
{
    if (m<0)
    {
        return INF; 
    } 
    else if (m == 0)
    {
        return 0;
    }
    else
    {
        int min = INF;
        for (int i = 0; i < n ; i++){
            int current = bestNTot(m-v[i]) +1;
            if (current<min){
                min=current;
            }
        }
        return min;
    }
}
    
int main(int argc, const char *argv[]) 
{
    if (argc == 1){
        cout << "entre une valeur sahbe" << endl;
    }
    else {
        int m = atoi(argv[1]);
        cout << "pour rendre " << m << " euro, il faut " << bestNTot(m) << " pièces." << endl;
    }
    return 0;
}
