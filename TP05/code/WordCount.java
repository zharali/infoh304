import java.io.*;
import java.util.*;
public class WordCount
{
	public static void main(String[] args)
	{
		if(args.length!=1)
		{
			System.out.println("Did you forget to include the name of the text file in the argument?");
			System.exit(-1);
		}
        
        try{
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            String line,line2;
            TreeMap<String,Integer> vocab=new TreeMap<String,Integer>();
            while ((line=reader.readLine())!=null)
            {
            	line2=line.toLowerCase().replaceAll("[^a-z]+", " ");
                String[] words=line2.split(" ");
                for(int i=0;i<words.length;i++)
                {
                	if(!words[i].isEmpty())
                    {
                		if(!vocab.containsKey(words[i]))
                        {
                            vocab.put(words[i],1);
                        }
                        else
                        {
                            vocab.put(words[i],vocab.get(words[i]).intValue()+1);
                        }
                    }
                    
                }
            }
            reader.close();
            System.out.println("Total number of words in \""+ args[0]+"\" : "+vocab.size());
            String countFileName="word_count_"+args[0];
            PrintWriter pw=new PrintWriter(countFileName);
            pw.println("Word count for "+args[0]+" :");
            pw.println("");
            Iterator<String> it=vocab.keySet().iterator();
            String word;
            int count;
            while(it.hasNext())
            {
                word=it.next();
                count=vocab.get(word);
                pw.println(word+" occured "+count+" times.");
            }
            
            pw.close();
            System.out.println("Word count file \""+countFileName+"\" created.");
        }
        catch (IOException x)
        {
            x.printStackTrace();
        }
	}
    
}