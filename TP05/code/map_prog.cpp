#include <iostream>
#include <string>
#include <fstream>
#include <map>
using namespace std;

void printMap(const map<string,int> & c, ostream & out = cout)
{
    map<string,int>::const_iterator itr;
    for (itr = c.begin(); itr!= c.end(); ++itr)
        out << itr->first << " : " << itr->second << endl;  //imprimer les deux, mot + cle
}

int main(int argc, char *argv[])
{
     ifstream finput (argv[1]);
     map<string,int> dictionnaire;
    if (finput.is_open())
    {
        pair<map<string,int>::iterator,bool> ret;
        string mot;
        while (finput >> mot)
        {
            ret=dictionnaire.insert(pair<string,int>(mot,1)); //recupere val de retour 
            if (ret.second == false)
                ++(ret.first->second);  //first et second pcq 2 paires, ret = paire itr bool, qd insertion rate bool =false pointe vers mot dans set, on veut incrementer, ret.first=itr vers obj dans map -> obj ds map = paire,on veut incrementer donc avoir acces a second 
        } 
        finput.close();
        printMap(dictionnaire);
        cout << "nb de mots uniques: " << dictionnaire.size() << endl;
    }
    else
    {
        cout << "impossible d'ouvrir le fichier d'entree" << endl;
        return 1;
    }
    ofstream foutput (argv[2]);
    if (foutput.is_open())
    {
        printMap(dictionnaire,foutput);
        foutput.close();
    }
    else
    {
        cout << "impossible d'ouvrir le fichier de sortie" << endl;
        return 1;
    }
    
    return 0;

}
