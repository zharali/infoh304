#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct noeud_t
{
	char* data;
	struct noeud_t* suivant;
} noeud;

/* creer_noeud: renvoie un pointeur vers un nouveau noeud avec la valeur passée en paramètre */
noeud* creer_noeud(char* data);

/* inserer_noeud: insère un noeud avec la valeur passée en paramètre */
noeud* inserer_noeud(char* data, noeud* tete);

/* supprimer_tete: supprime le noeud de tête */
noeud* supprimer_tete(noeud* tete);

/* imprimer_liste: imprime la liste à l'écran */
void imprimer_liste(noeud* tete);

/* fimprimer_liste: imprime la liste dans un fichier */
void fimprimer_liste(noeud* tete, FILE* filename);
