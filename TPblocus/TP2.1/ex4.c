#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONGMAX 50


int main(int argc, char* argv[])
{
	if (argc == 1)  //1er arg = nom fct
	{
		printf("Erreur, indiquez nom du fichier \n");
		return 1;
	}
	
	printf("Ouverture fichier \n");
	FILE* fichier;
	
	if ((fichier=fopen(argv[1],"r="))==NULL)   // r+ = r and w 
	{
		printf("Impossible d'ouvrire le fichier \n");
		return 2;
	}
	
	printf("Fichier ok \n");
	
	int i=0;
	char mot[LONGMAX];
	
	while(fscanf(fichier, "%s", mot)!= EOF)
		i++;
		
	fprintf(fichier, "Le nombre de mot est : %d \n", i);  //print le nb de mot dans le fichier
		
	fclose(fichier);
		
	//autant tout faire en une fois et pas fermer puis rouvrir le fichier  -> pb : supprime tout le texte 
	/*if ((fichier=fopen(argv[1], "w"))==NULL)
	{
		printf("Impossible d'écrire dans le fichier \n");
		return 3;
	}
	
	printf("Ecriture possible \n");
	fprintf(fichier, "Le nombre de mot est : %d \n", i);
	
	fclose(fichier);*/
	
	printf("Le nombre de mot est : %d \n", i);
	return 0;	
}
