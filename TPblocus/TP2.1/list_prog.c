#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#define LONGMAX 50


int main(int argc, char* argv[])
{
	noeud* tete = NULL; 
	if (argc < 3)  //1er arg = nom fct
	{
		printf("Erreur, indiquez nom des 2 fichiers \n");
		return 1;
	}
	
	printf("Ouverture fichier d'entrée\n");
	FILE* finput;
	if ((finput=fopen(argv[1],"r"))==NULL)   // r+ = r and w 
	{
		printf("Impossible d'ouvrire le fichier in\n");
		return 2;
	}
	printf("Fichier in ok \n");
	
	FILE* foutput;
	if ((foutput=fopen(argv[2],"w"))==NULL)
	{
		printf("impossible ouvrire fichier out\n");
		return 3;
	}
	printf("Fichier out ok \n");

	int i=0;
	char mot[LONGMAX];
	
	while(fscanf(finput, "%s", mot)!= EOF)
	{
		tete=inserer_noeud(mot,tete);
		i++;
	}
	
	fimprimer_liste(tete,foutput);  //print le nb de mot dans le fichier	
	fclose(finput);	
	fclose(foutput);	
	printf("Le nombre de mot est : %d \n", i);
	return 0;	
}



/*#include "list.h"

int main()
{
	noeud* tete = NULL; //initialiser le pointeur a NULL
	imprimer_liste(tete);
	
	tete=inserer_noeud("premier",tete);
	imprimer_liste(tete);
	
	tete=inserer_noeud("deuxieme",tete);
	imprimer_liste(tete);	
	
	tete=supprimer_tete(tete);
	imprimer_liste(tete);	
	
	tete=supprimer_tete(tete);
	imprimer_liste(tete);
	
	tete=supprimer_tete(tete);
	imprimer_liste(tete);
	
	return 0;
}*/
