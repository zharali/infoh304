#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#define LONGMAX 50


int main(int argc, char* argv[])
{
	noeud* tete = NULL; 
	if (argc == 1)  //1er arg = nom fct
	{
		printf("Erreur, indiquez nom du fichier \n");
		return 1;
	}
	
	printf("Ouverture fichier \n");
	FILE* fichier;
	
	if ((fichier=fopen(argv[1],"r+"))==NULL)   // r+ = r and w 
	{
		printf("Impossible d'ouvrire le fichier \n");
		return 2;
	}
	
	printf("Fichier ok \n");
	
	int i=0;
	char mot[LONGMAX];
	
	while(fscanf(fichier, "%s", mot)!= EOF)
	{
		tete=inserer_noeud(mot,tete);
		i++;
	}
	
	imprimer_liste(tete);
	fprintf(fichier, "Le nombre de mot est : %d \n", i);  //print le nb de mot dans le fichier	
	fclose(fichier);	
	printf("Le nombre de mot est : %d \n", i);
	return 0;	
}
