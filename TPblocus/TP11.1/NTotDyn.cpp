#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

//tab des pieces :
vector<int> v = {1,3,4,10};
int n = v.size();

//tab memoisation :
vector<int> nTot;
vector<bool> memoized;

//tab meilleurs choix : 
vector<int> bestChoice;

int NTotDyn(int m)
{
	if(m<0){
		return INFINITY;}
	else if (m==0){
		return 0;}
	else if (memoized[m])
	{return nTot[m];}
	else
	{
		int min = INFINITY;
		for (int i=0; i<n;i++){
			int current = NTotDyn(m-v[i])+1;
			if (current<min){
				min=current;
				bestChoice[m]=i;
			}
		}
		nTot[m]=min;
		memoized[m]=true;
		return min;
	}
}


int main(int argc,const char *argv[])
{
	if(argc == 1){
		cout<<"veuillez entrer un montant"<<endl;
	}
	else{
		int montant = atoi(argv[1]);   //changer param en entier
		//initialiser tab memo :
		nTot = vector<int>(montant+1,0);
		memoized = vector<bool>(montant+1, false);
		bestChoice = vector<int>(montant+1,0);
		int solution = NTotDyn(montant);
		int i = 1;
		int p = montant;
		while(p>0)
		{
			cout<<"piece "<<i++<<" : "<<v[bestChoice[p]]<<endl;
			p -= v[bestChoice[p]];
		}
		if (solution >= INFINITY)
			cout<<"pas de solution"<<endl;
		else
			cout<<"le nombre de piece pour le montant "<<montant<<" est "<<solution<<endl;
		}

}
