#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

//tab des pieces 
vector<int> v = {1,3,4,10};
int n = v.size();

int NTotRechExh(int m)
{
	if(m<0){
		return INFINITY;}
	else if (m==0){
		return 0;}
	else
	{
		int min = INFINITY;
		for (int i=0; i<n;i++){
			int current = NTotRechExh(m-v[i])+1;
			if (current<min){min=current;}
		}
		return min;
	}
}
/*L’algorithme ayant une complexité exponentielle, vous observerez qu’il devient rapidement lent
lorsque le montant devient élevé, contrairement à l’algorithme de l’exercice 1. Néanmoins, cet algorithmeci a l’avantage de toujours renvoyer le nombre de pièces minimum correct.
*/



int main(int argc,const char *argv[])
{
	if(argc == 1){
		cout<<"veuillez entrer un montant"<<endl;
	}
	else{
		int montant = atoi(argv[1]);   //changer param en entier
		int solution = NTotRechExh(montant);
		if (solution >= INFINITY)
			cout<<"pas de solution"<<endl;
		else
			cout<<"le nombre de piece pour le montant "<<montant<<" est "<<solution<<endl;
		}

}
