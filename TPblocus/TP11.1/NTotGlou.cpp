#include <iostream>
#include <cstdlib>
#include <vector>
#define INFINITY 10000
using namespace std;

//tab des pieces 
vector<int> v = {1,3,4,10};
int n = v.size();

int NTotGlou(int m)
{
	if(m<0){
		return INFINITY;}
	else if (m==0){
		return 0;}
	else
	{
		int i = 0;  //attention bien mettre le compteur à 0 
		while(i < n-1 && v[i+1] <= m){
			i+=1;}
		return NTotGlou(m-v[i]) +1;
	}
}


int main(int argc,const char *argv[])
{
	if(argc == 1){
		cout<<"veuillez entrer un montant"<<endl;
	}
	else{
		int montant = atoi(argv[1]);   //changer param en entier
		int solution = NTotGlou(montant);
		if (solution >= INFINITY)
			cout<<"pas de solution"<<endl;
		else
			cout<<"le nombre de piece pour le montant "<<montant<<" est "<<solution<<endl;
		}

}
