#include "noeud.h"

class Liste
{
private:
	int nombreNoeuds;
	Noeud* tete;
public:
	Liste();
	~Liste();
	
	void insere(string chaine);
	void supprime();
	void imprimeListe(std::ostream & out=std::cout) const;
	void insereTrie(string chaine);
	int getNombreNoeuds() const;
	Noeud* getTete() const;
};
