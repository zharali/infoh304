#include <iostream>
#include <string>
#include <fstream>
#include "list.h"
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;

int main(int argc, char *argv[])
{
	ifstream finput (argv[1]);
	Liste liste;
	if( finput.is_open() )
	{
		string mot;
		while( finput >> mot )
			liste.insereTrie(mot);		
		finput.close();
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	ofstream foutput (argv[2]);
	if (foutput.is_open())
	{
		liste.imprimeListe(foutput);
		foutput.close();
		return 0;
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
}

/*#include "list.h"

int main()
{
	Liste liste;
	liste.insereTrie("Hello");
	liste.imprimeListe();
	liste.insereTrie("World");
	liste.imprimeListe();
	liste.insereTrie("Salut");
	liste.imprimeListe();
	liste.supprime();
	liste.imprimeListe();
	liste.supprime();
	liste.imprimeListe();
	liste.supprime();
	liste.imprimeListe();
	return 0;
}*/
