#include "list.h"
using std::cout;
using std::endl;
using std::ostream;


Liste::Liste()    //constructeur (initialiser)
{
	nombreNoeuds=0;
	tete=NULL;
}

Liste::~Liste()   //destructeur
{
	while(tete!=NULL)
		supprime();
}

void Liste::insere(string chaine)
{
	Noeud* n=new Noeud(chaine);
	n->setSuivant(tete);
	tete=n;
	nombreNoeuds++;
}

void Liste::insereTrie(string chaine)
	/*
 * 4 cas different :
 * liste vide
 * inserer en tete
 * inserer en fin
 * inserer au milieu
 * */
{
	//cas 1 et 2: liste vide, inserer en tete (meme op)
	if (tete==NULL || tete->getData() > chaine)
	{
		Noeud* n=new Noeud(chaine);
		n->setSuivant(tete);
		n->incrementeCompteur();
		tete=n;
		nombreNoeuds++;
	}
	
	//cas 3 et 4: inserer milieu ou fin -> cad apres un noeud existant
	else
	{
		Noeud* temp=tete;
		while ((temp->getSuivant() != NULL) && (temp->getData() <= chaine))  //tant qu'on est pas a la fin ou qu'on a trouve la place
			temp=temp->getSuivant();                                         //on continu a avancer
		if (temp->getData()!=chaine)  //pas avoir deux fois le meme mot, placement 
		{
			Noeud* n=new Noeud(chaine);
			n->setSuivant(temp->getSuivant());
			n->incrementeCompteur();
			temp->setSuivant(n);
			nombreNoeuds++;
		}
		else
		{
			//mot deja present
			temp->incrementeCompteur();
		}
	}
}

void Liste::supprime()
{
	if (tete!=NULL)
	{
		Noeud* temp=tete;
		tete=tete->getSuivant(); //nvlle tete = le suivant de la tete actuelle 
		delete temp;
		nombreNoeuds--;
	}
	else
		cout<< "Liste vide" << endl;
		
}

void Liste::imprimeListe(ostream & out) const
{
	Noeud* temp=tete;
	cout<< "Liste avec " << nombreNoeuds << " éléments" << endl;
	while(temp!=NULL)
	{
		cout<< "Mot: " << temp->getData() << endl;	
		cout<< "Nombre d'occurence du mot : " << temp->getCompteur() <<endl;
		temp=temp->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete() const
{
	return tete;
}

