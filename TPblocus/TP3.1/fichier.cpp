#include <iostream>
#include <string>
#include <fstream>
#include "list.h"
using std::cout;
using std::endl;
using std::string;
using std::ifstream;

int main(int argc, char *argv[])
{
	ifstream fichier (argv[1]);
	Liste liste;
	if( fichier.is_open() )
	{
		string mot;
		while( fichier >> mot )
			liste.insere(mot);
		liste.imprimeListe();
		fichier.close();
	}
	else
		cout << "Impossible d'ouvrir le fichier" << endl;
		
	return 0;
	
}
