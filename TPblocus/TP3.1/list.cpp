#include "list.h"
using std::cout;
using std::endl;

Liste::Liste()    //constructeur (initialiser)
{
	nombreNoeuds=0;
	tete=NULL;
}

Liste::~Liste()   //destructeur
{
	while(tete!=NULL)
		supprime();
}

void Liste::insere(string chaine)
{
	Noeud* n=new Noeud(chaine);
	n->setSuivant(tete);
	tete=n;
	nombreNoeuds++;
}

void Liste::supprime()
{
	if (tete!=NULL)
	{
		Noeud* temp=tete;
		tete=tete->getSuivant(); //nvlle tete = le suivant de la tete actuelle 
		delete temp;
		nombreNoeuds--;
	}
	else
		cout<< "Liste vide" << endl;
		
}

void Liste::imprimeListe() const
{
	Noeud* temp=tete;
	cout<< "Liste avec " << nombreNoeuds << " éléments" << endl;
	while(temp!=NULL)
	{
		cout<< "Mot: " << temp->getData() << endl;
		temp=temp->getSuivant();
	}
}

int Liste::getNombreNoeuds() const
{
	return nombreNoeuds;
}

Noeud* Liste::getTete() const
{
	return tete;
}

