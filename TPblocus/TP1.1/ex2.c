#include <string.h>
#include <stdio.h>
#define LONGMAX 30

/* inverse: inverse le sens du mot */
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}

int main()
{
	char mot[LONGMAX+1];
	int i;
	scanf("%s",mot);
	i = strlen(mot);

	printf("Le mot %s est de longueur %i\n", mot, i);
	inverse(mot);
	printf("Le mot inverse est %s\n",mot);
	return 0;
}

/* scanf s'arrete des qu'il y a un espace, pex si on entre "hello world" il lira que "hello" 
