#include <string.h>
#include <stdio.h>
#define LONGMAX 30

/* inverse: inverse le sens du mot */
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}

/* maj_to_min: remplace toute les majuscules par des minusclues*/
void maj_to_min(char c[])
{
	int i;
	for (i=0; c[i]!= '\0'; i++)
	{
		if (c[i] >= 'A' && c[i] <= 'Z')  //ATTENTION 'A' et pas "A" 
			c[i] += 'a'-'A';
	}
}

int main()
{
	char mot[LONGMAX+1];
	int i;
	scanf("%s",mot);
	i = strlen(mot);

	printf("Le mot %s est de longueur %i\n", mot, i);
	inverse(mot);
	printf("Le mot inverse est %s\n",mot);
	maj_to_min(mot);
	printf("Mot en min est %s\n", mot);
	return 0;
}

