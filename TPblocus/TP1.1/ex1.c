#include <string.h>
#include <stdio.h>
#define LONGMAX 30

/* inverse: inverse le sens du mot */
void inverse(char s[])
{
	char temp;
	int i,j;
	for ( i=0, j=strlen(s)-1; i<j; i++, j-- )
	{
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
}

int main()
{
	char c, mot[LONGMAX+1];
	int i;
	for ( i=0; i<LONGMAX; i++ )
	{
		c = getchar();
		if ( c>='a' && c<='z' )
			mot[i] = c;
		else
			break;
	}
	mot[i] = '\0';
	printf("Le mot %s est de longueur %i\n", mot, i);
	inverse(mot);
	printf("Le mot inverse est %s\n",mot);
	return 0;
}

/*Si l’on entre un mot plus long que LongMax (ici 30 caractères), le mot sera coupé vu que la boucle for
à la ligne 22 s’arrête dès que 30 caractères ont été lus.
Si l’on retire le test i<LongMax, le fichier compile correctement car il est syntaxiquement correct, mais
on observe des erreurs à l’exécution : le mot n’est pas correctement affiché, et sa longueur ne correspond
pas au nombre de caractères entrés. Ces erreurs sont due à un dépassement de tampon*/
