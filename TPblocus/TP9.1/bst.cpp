#include <iostream>

#include <string>

#include <chrono>

#include <thread>

#include <random>

#include <unordered_set>

extern "C" {

#include <UbigraphAPI.h>

}



using namespace std;



class BinarySearchTree

{

private:

	struct node

	{

		node *left;

		node *right;

		node *parent;

		int content;

	};

	node* root;

	node* tree_search(int content);

	node* tree_search(int content, node* location);

	void delete_no_child(node* location);

	void delete_left_child(node* location);

	void delete_right_child(node* location);

	void delete_two_children(node* location);

	

public:

	BinarySearchTree ()

	{

		root=NULL;

	}

	bool isEmpty()

	{

		return root==NULL;

	}

	void insert_element(int content);

	void delete_element(int content);

	void inorder(node* location);

	void print_inorder();

};





void BinarySearchTree::insert_element(int content)

{

	//New node

	node* n = new node();

	n->content = content;

	n->left = NULL;

	n->right = NULL;

	n->parent = NULL;

	

	//For visualization

	int eid,vid;

	this_thread::sleep_for(chrono::milliseconds(100));

	ubigraph_new_vertex_w_id(content);

	ubigraph_set_vertex_attribute(content, "color", "#0000ff");

	ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());

	

	if(isEmpty())

	{

		root = n;

		ubigraph_set_vertex_attribute(content, "color", "#ff0000");

		this_thread::sleep_for(chrono::milliseconds(100));

		ubigraph_set_vertex_attribute(content, "color", "#0000ff");

		this_thread::sleep_for(chrono::milliseconds(100));

	}

	else

	{

		

		node* pointer = root;

		

		while ( pointer != NULL )

		{

			n->parent = pointer;

			

			if(n->content > pointer->content)

			{

				vid = pointer->content;

				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");

				this_thread::sleep_for(chrono::milliseconds(100));

				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");

				this_thread::sleep_for(chrono::milliseconds(100));

				

				pointer = pointer->right;

				

			}

			else

			{

				vid = pointer->content;

				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");

				this_thread::sleep_for(chrono::milliseconds(100));

				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");

				this_thread::sleep_for(chrono::milliseconds(100));

				

				pointer = pointer->left;

				

			}

		}

		

		if ( n->content < n->parent->content )

		{

			n->parent->left = n;

			this_thread::sleep_for(chrono::milliseconds(200));

			eid = ubigraph_new_edge(n->parent->content, content);

			ubigraph_set_edge_attribute(eid, "oriented", "true");

			

		}

		else

		{

			n->parent->right = n;

			this_thread::sleep_for(chrono::milliseconds(200));

			eid = ubigraph_new_edge(n->parent->content, content);

			ubigraph_set_edge_attribute(eid, "oriented", "true");		

		}

	}

}



void BinarySearchTree::inorder(node* location)

{

	//print par recursivite !! (arbre bin, clee plus gde tjrs a droite)

	if (location!=NULL)

	{

		inorder(location->left);

		cout <<" "<< location->content;

		inorder(location->right);

	}

	/*if(location==NULL)

		cout << "arbre vide" << endl;

	else

	{

		if(location->left > location && location->right < location)

		{	

			cout<< location->content << endl;

			location=location->left;

		}

		else if (location->left < location && location->right > location)

		{

			cout<<location<<endl;

			location=location->right;

		}

	}*/

	

}



void BinarySearchTree::print_inorder()

{

	cout << "Contenu de l'arbre :";

	inorder(root);

	cout << endl;

}



BinarySearchTree::node* BinarySearchTree::tree_search(int content)

{

	return tree_search(content, root);

}



BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)

{

	if(location==NULL)

		cout<<"arbre vide"<<endl;

	while(location!=NULL)

	{

		if(location->content==content)

		{

			cout <<"noeud trouvé"<<endl;

			return location;

		}

		else if (location->content > content)

			location=location->left;

		else if (location->content < content)

			location=location->right;

	}

	cout<<"noeud pas trouvé"<<endl;

		

	return NULL;

}



void BinarySearchTree::delete_no_child(node* location)

{

	cout<<"no child"<<endl;

	//supp la racine

	if(location=root)

	{

		root=NULL;

		delete location;

	}

	else if (location->parent->left==NULL && location->parent->right==location)

	{

		location->parent->right=NULL;

		delete location;

	}

	else if (location->parent->right==NULL && location->parent->left==location)

	{

		location->parent->left=NULL;

		delete location;

	}

	

}



void BinarySearchTree::delete_left_child(node* location)

{

	cout<<"left child"<<endl;

	//supp la racine : remplacer la racine par l'enfant de gauche

	if(location==root)

	{

		root=location->left;

		location->left->parent=NULL;

		delete location;

	} 

	else if (location->parent->left!=NULL && location->parent->left==location)

	{	

		location->parent->left=location->left;

		location->left->parent=location->parent;

		delete location;

	}

	else

	{

		location->parent->right=location->left;

		location->left->parent=location->parent;

		delete location;	

	}

}



void BinarySearchTree::delete_right_child(node* location)

{

	cout<<"right child"<<endl;

	//supp la racine : remplacer la racine par l'enfant de droit

	if(location==root)

	{

		root=location->right;

		location->right->parent=NULL;

		delete location;

	} 

	else if (location->parent->right!=NULL && location->parent->right==location)

	{	

		location->parent->right=location->right;

		location->right->parent=location->parent;

		delete location;

	}

	else

	{

		location->parent->left=location->right;

		location->right->parent=location->parent;

		delete location;	

	}

}





void BinarySearchTree::delete_two_children(node* location)

/*remplacez le contenu du noeud avec celui de son successeur dans un

parcours dans l’ordre (qui est dans ce cas l’élément le plus petit de son sous-arbre droit), puis

supprimez ce successeur avec les règles pour les deux premiers cas*/

{

	cout<<"two children"<<endl;

	node* successor=location->right; //successeur = elem plus ptt arbre droite

	node* new_node;

	node* left_child=location->left; //assigner un node pour ecriture plus facile sinon tt le temps ecrire location->...

	while(left_child != NULL) //tant qu'on est pas a la fin de l'arbre on continue a avancer

	{

		successor=left_child;

		left_child=successor->left;

	}

	int new_content = successor->content;

	int old_content = location->content;

	

	if (successor->right == NULL)

	{//successor n'a pas de child -> = leaf

			delete_no_child(successor);

	}

	else

	{//successor a un right child

		delete_right_child(successor);	

	}

	

	location->content=new_content; //mettre le contenu du successeur dans ce qu'on vient de supp

	

}



void BinarySearchTree::delete_element(int content)

{

	node* location = tree_search(content);

	//3 cas differents

	if(location==NULL)

		cout<<"pas trouve"<<endl;

	else if (location->left==NULL && location->right==NULL)

		delete_no_child(location);

	else if (location->left!=NULL && location->right==NULL)

		delete_left_child(location);

	else if (location->right!=NULL && location->left==NULL)

		delete_right_child(location);

	else 

		delete_two_children(location);

			

}





int main()

{

	ubigraph_clear();

	unordered_set<int> vertices;   //pr pas avoir les mm val

	int vertex;

	BinarySearchTree bst;

	

	random_device rd;

	mt19937 gen(rd());
	uniform_int_distribution<int> r(0, 1000);

	for ( int i=1; i<=10; i++ )
	{
		vertex = r(gen);  //r = nb aléatoirement distribué 
		if ( vertices.count(vertex) == 0 )
		{
			vertices.insert(vertex);
			//bst.insert_element(vertex);  //ou on peut juste utiliser ca plsrs fois pr remplir
		}
		else
			i--;
	}

		//insertion 10 elem arbre desequilibré :
	/*bst.insert_element(10);
	bst.insert_element(9);
	bst.insert_element(8);
	bst.insert_element(7);
	bst.insert_element(6);
	bst.insert_element(5);
	bst.insert_element(4);
	bst.insert_element(3);
	bst.insert_element(2);
	bst.insert_element(1);*/

	//insertion 10 elem arbre equil :
	bst.insert_element(5);
	bst.insert_element(10);
	bst.insert_element(4);
	bst.insert_element(9);	
	bst.insert_element(3);
	bst.insert_element(8);
	bst.insert_element(2);
	bst.insert_element(7);
	bst.insert_element(1);
	bst.insert_element(6);

	//ex3
	bst.print_inorder();

	//ex4
	bst.delete_element(2);
	bst.print_inorder();

	

	//ex5

	/*Le successeur d’un élément dans un parcours infixe est le plus petit élément dans le sous-arbre droit,

qui est effectivement le noeud “le plus à gauche” dans le sous-arbre droit. Donc il n’a pas d’enfant gauche

(car il est déjà “le plus à gauche”).*/

	

	return 0;

}

