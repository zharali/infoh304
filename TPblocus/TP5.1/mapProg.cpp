#include <iostream>
#include <string>
#include <fstream>
#include <map>
using namespace std;

void printMap(const map<string,int> & s, ostream & out=cout)
{
	map<string,int>::const_iterator itr;
	for (itr=s.begin(); itr!=s.end(); ++itr)
		out<<itr->first<<" : "<<itr->second<<endl; //deux valeur dans un set
}

int main(int argc, char *argv[])
{
	map<string,int> liste;
	ifstream finput (argv[1]);
	if( finput.is_open() )
	{	
		pair<map<string,int>::iterator,bool> ret;
		string mot;
		int i=0;
		while( finput >> mot )
		{
			liste.insert(pair<string,int>(mot,i+1));	
			++liste[mot];  //Maj nb de fois qu'un mot apparait 
			
			/*
			 * ou :
			 *  ret=dictionnaire.insert(pair<string,int>(mot,1)); //recupere val de retour 
				if (ret.second == false)
                ++(ret.first->second); 
			 * */
		}
		finput.close();
		printMap(liste);
		cout<<"Nombre de mots uniques : "<<liste.size()<<endl;
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	ofstream foutput (argv[2]);
	if (foutput.is_open())
	{
		printMap(liste,foutput);
		foutput.close();
	}
	
	return 0;
}

/*Le programme “list_prog” réalisé lors de la séance 3 prend beaucoup plus de temps. La raison est
que pour chaque insertion, le programme doit parcourir la liste chaînée à partir du début jusqu’à trouver
la position où insérer le mot. Dans le pire cas, on doit parcourir toute la liste, soit un temps Θ(n) pour
insérer un élément dans une liste de longueur n. Pour insérer un total de n mots, on a donc un coût en
Θ(n
2
), le programme fonctionnant essentiellement comme l’algorithme de tri par insertion.
Les deux autres programmes sont nettement plus rapides. Pour le programme C++ “map_prog”, le
conteneur p[+ est typiquement implémenté par un arbre binaire de recherche équilibré (voir cours 9),
tout comme la classe 2(kk][+ en Java. Donc, pour n éléments dans le p[+, un insertion prend au plus un
temps Θ(log n), et pour n mots dans le fichier on a un coût total en Θ(n · log n).*/
