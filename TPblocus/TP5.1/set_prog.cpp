#include <iostream>
#include <string>
#include <fstream>
#include <set>
using namespace std;

void printSet(const set<string> & c, ostream & out = cout)
{
    set<string>::const_iterator itr;
    for (itr = c.begin(); itr!= c.end(); ++itr)
        out << *itr << endl;
}

int main(int argc, char *argv[])
{
    ifstream finput (argv[1]);
    if (finput.is_open())
    {
        set<string> liste;
        string mot;
        while (finput >> mot)
            liste.insert(mot);
        finput.close();
        printSet(liste);
        cout << "nb de mots uniques: " << liste.size() << endl;
    }
    else
    {
        cout << "impossible d'ouvrir le fichier" << endl;
        return 1;
    }
    return 0;

}