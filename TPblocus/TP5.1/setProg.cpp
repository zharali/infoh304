#include <iostream>
#include <string>
#include <fstream>
#include <set>
using namespace std;

void printSet(const set<string> & s, ostream & out=cout)
{
	set<string>::const_iterator itr;
	for (itr=s.begin(); itr!=s.end(); ++itr)
		out<<*itr<<endl;
}

int main(int argc, char *argv[])
{

	ifstream finput (argv[1]);
	if( finput.is_open() )
	{	
		set<string> liste;
		string mot;
		while( finput >> mot )
			liste.insert(mot);	
		finput.close();
		printSet(liste);
		cout<<"Nombre de mots uniques : "<<liste.size()<<endl;
	}
	else
	{
		cout << "Impossible d'ouvrir le fichier" << endl;
		return 1;
	}
	
	return 0;
}
