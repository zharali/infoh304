#ifdef OSX
#include <GLUT/glut.h>
#elif LINUX
#include <GL/glut.h>
#endif
#include <iostream>
#include <string>
#include <set>
#include <utility>
#include <random>
#include <fstream>
#include <boost/functional/hash.hpp>

using namespace std;

set<string> words;
GLint* points;
int size = 0;

void init(float r, float g, float b)
{
	glClearColor(r,g,b,0.0);  
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D (1.0, 128.0, 1.0, 128.0);
}

unsigned long long prehach_function(string word)
{
	unsigned long long entier=0;
	for (int i=0; i<word.length(); i++)
	{
		entier += (unsigned long long)word[i] << 7*i;  //type cast (unsigned long long) ; 
	}/*Comme chaque caractère imprimable est en fait un entier entre 0 et 127 (code ASCII), on peut voir
un mot comme un entier écrit en base 128
un mot de longueur l peut générer un entier allant jusqu’à 128l − 1 = 2⁷^l − 1, d’où l’utilité du type
unsigned long long, qui peut coder des entiers jusque 264 − 1*/
	return entier;
	
}

pair<int,int> hash_function(string word)   
{
	
	unsigned long long entier = prehach_function(word);
	//unsigned long long hash;
	int x,y;
	pair<int,int> coord;
	
	//hachage par division
	int m = 91234365869;
	//hash = entier % m;
	
	//hachage par mult
	int a = 3541164156957;
	int w = 64;
	int r = 14;
	//hash = (a*entier) % (unsigned long long)pow(2,w) >> (w-r); -> incorrecte
	//hash = (a*entier) >> (w-r);
	/*On prend w = 64 car un entier de type unsigned long long est codé sur 64 bits, et r = 14 car on
veut une empreinte avec au moins 14 bits (7 pour x et 7 pour y).
• L’entier 264 contenant 19 chiffres, on choisit a tout aussi long. Une valeur plus haute que 264
provoquerait une erreur de compilation (valeur pas acceptable pour un entier de type H:"80:Z.
Q3:0 Q3:0), et une valeur significativement plus basse n’exploiterait pas bien tous les bits de la
valeur de préhachage, et mènerait donc à plus de collisions (donc un aspect moins dense de la
figure).
• Il n’y a pas besoin de prendre le modulo 2w
après la multiplication par a : en effet, on travaille
avec des entiers de 64 bits, et donc toutes les opérations sont calculées modulo 264. De plus, notez
qu’essayer de calculer q3OP!p<<N provoquerait une erreur de compilation vu que c’est un entier
trop grand pour le type H:"80:Z. Q3:0 Q3:0 (la valeur autorisée la plus grande étant 264 − 1).*/
	
	//hachage universel
	int p = 9892777; //nb premier
	int c = 9892621; //nb premier dans p
	int b = 9894107; //nb premier dans p
	//hash = (c*entier+b) % p;
	
	//hachage boost 
	 /*l’aspect est maintenant très similaire à la fonction aléatoire, ce qui
est précisément le but d’une bonne fonction de hachage*/
	boost::hash<string> boost_hash;
	size_t hash = boost_hash(word);
	
	
	x = hash % 128 + 1;
	y = (hash/128) % 128 + 1;
	/*Notez qu’à partir de la valeur fournie par la fonction de hachage, on doit tirer deux entiers x, y entre
1 et 128. Pour x, il suffit de prendre )[") u !!r (où u est l’opérateur modulo), ce qui renvoie un entier
entre 0 et 127, puis ajouter 1. Notez que 128 = 2^7
, donc )[") u !!r correspond aux 7 bits les moins
significatifs de )[").
Pour y, on divise d’abord )[") par 128 pour supprimer les 7 bits les moins significatifs (ils se
retrouvent derrière la virgule et sont donc tronqués pour une division entière). On effectue ensuite la
même opération que pour x pour extraire les 7 bits suivants.*/
	
	coord = make_pair(x,y);
	return coord;
	
	/*//renvoie paire d'entier aleatoire
	int x, y;
	pair<int,int> coord;
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> r(1, 128);
	x = r(gen);
	y = r(gen);
	coord = make_pair(x,y);
	return coord;*/
}

void make_points()
{
	pair<int,int> coord;
	int i=0;
	for (set<string>::iterator it=words.begin(); it!=words.end(); ++it)
	{
		coord = hash_function(*it);
		points[i++] = (GLint)coord.first;
		points[i++] = (GLint)coord.second;	
	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glViewport(128.0,128.0,512.0,512.0);
	glPointSize(2.0f);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_INT, 0, points);
	glDrawArrays(GL_POINTS,0,size);
	glDisableClientState(GL_VERTEX_ARRAY);
	glutSwapBuffers();
}

int main(int argc,char *argv[])
{
	
	string word;
	
	if ( argc != 2 )
	{
		cout << "Enter a file name." << endl;
	}
	else
	{
		ifstream file (argv[1]);
		
		string str;
		while ( file >> word )
		{
			words.insert(word);
		}
		size = words.size();
		cout << "Document size: " << words.size() << " words" << endl;

		points = (GLint *)malloc(2*size*sizeof(GLint));
		make_points();
		glutInit(&argc,argv);
		glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
		glutInitWindowSize (768, 768);
		glutInitWindowPosition (200, 200);
		glutCreateWindow ("Hash Function Visualization");
		init(0.0,0.0,0.0);
		glutDisplayFunc(display);
		glutMainLoop();
	}
	
	return 0;
}
