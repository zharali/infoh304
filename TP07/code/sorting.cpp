#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <chrono>
#include <random>

using namespace std;
using namespace std::chrono;

typedef high_resolution_clock clk;

void merge(vector<string> & l, int left, int mid, int right)
{
	/** Merge two sorted vectors **/
	/*int nl = mid - left;
	int nr = right - mid + 1;*/
	vector<string> ll;
	vector<string> lr;
	ll.insert(ll.begin(),l.begin()+left,l.begin()+mid);
	lr.insert(lr.begin(),l.begin()+mid,l.begin()+right+1);
	//pr mettre ll=infini mettre des zzz, mot qui vient en dernier par ordre alphabetique
	ll.push_back("zzzz");
	lr.push_back("zzzz");
	int il = 0;
	int ir = 0;
	for (int i=left; i<=right; i++)
	{
		if (ll[il]<=lr[ir])
		{
			l[i]=ll[il];
			il+=1;
		}	
		else
		{
			l[i]=lr[ir];
			ir+=1;
		} 
	}

}

void insertion_sort(vector<string> & l, int left=0, int right=-1)  //modif pour pvr trier des ss tableaux
{
	if (right<0)
		right=l.size()-1;
	string tmp;
	for(int i=left; i<=right; i++)
	{
		int j=i;
		while ( j>0 && l[j-1]>l[j] ) 
		{
			tmp = l[j];
			l[j] = l[j-1];
			l[j-1] = tmp;
			j--;
		}
	}
}

void merge_sort(vector<string> & l, int left, int right)
{
	/** Sort a vector by calling merge() recursively **/
	if (right-left<150) //switch au tri par insert pr ptt n car plus rapide, devient meilleur pr n faible mais - bon que tri rapide car tri rap = tri en place ca cst est plus faible 
		insertion_sort(l,left,right);
	else
	{
		int mid = ceil((float)(left+right)/2);
		merge_sort(l, mid,  right);
		merge_sort(l, left,  mid-1);
		merge(l,left, mid,  right);
	}
}



int check_sorted(vector<string> & l1, vector<string> & l2)
{
	int identical=0;
	for(int i=0; i<l1.size(); i++)
	{
		if( l1[i].compare(l2[i]) == 0 )
			identical++;      
	}
	cout << "Similarity between the two sorted lists: " << (identical/l1.size())*100 << "%" << endl;
	return (int)(identical/l1.size())*100;
}

vector<unsigned long long> compare_algorithms(vector<string> & l_merge, vector<string> & l_stl, vector<string> & l_insertion)
{
		vector<unsigned long long> result;
		nanoseconds time1, time2, time3;
		clk::time_point start, stop;
		for( int i=0; i<100000; i++)
		{
			start = clk::now();
			stop = clk::now();
		}
		cout << "Element count: " << l_merge.size() << endl;
		start = clk::now();
		merge_sort(l_merge,0,l_merge.size()-1);
		stop = clk::now();
		time1 = duration_cast<nanoseconds>(stop-start);
		cout << "Elapsed time for merge sort: " << (long)time1.count() << " nanoseconds" << endl;
		start = clk::now();
		sort(l_stl.begin(), l_stl.end());
		stop = clk::now();
		time2 = duration_cast<nanoseconds>(stop-start);
		cout << "Elapsed time for STL sort: " << (long)time2.count() << " nanoseconds" << endl;
		start = clk::now();
		insertion_sort(l_insertion);
		stop = clk::now();
		time3 = duration_cast<nanoseconds>(stop-start);
		cout << "Elapsed time for insertion sort: " << (long)time3.count() << " nanoseconds" << endl;
		check_sorted(l_merge,l_stl);
		
		result.push_back((unsigned long long)l_merge.size());
		result.push_back(time1.count());
		result.push_back(time2.count());
		result.push_back(time3.count());
		return result;

}

int main(int argc, char *argv[])
{
	string word;
	
	vector<string> l, l1, l2, l3;
	vector<unsigned long long> result;
	
	if ( argc!=3 )
	{
		cout << "Enter a file name and the maximum number of elements." << endl;
	}
	else
	{
		int max = atoi(argv[2]);
		ifstream file (argv[1]);
		
		const double resolution = double(clk::period::num) / double(clk::period::den);
		cout << "System clock info:" << endl;
		cout << "Clock period: "<< resolution*1e9 << " nanoseconds." << endl;
		ofstream data ("plot.dat");
		data << "#Element size\t#Merge Sort\t#STL Sort\t#Insertion Sort" << endl;
		string str;
		while ( file >> word )
		{
			l.push_back(word);
		}
		string temp;
		int steps = 1;
		int base = 2;
		random_device rd;
		mt19937 gen(rd());
		uniform_int_distribution<int> r(0, l.size());
		while ( l.size() > (int)pow(base,steps) && ((int)pow(base,steps) <= max) && ((int)pow(base,steps) < l.size()) )
		{
			for ( int i=1; i<=(int)pow(base,steps); i++ )
			{
				temp = l[r(gen)];
				l1.push_back(temp);
				l2.push_back(temp);
				l3.push_back(temp);
			}
			result = compare_algorithms(l1,l2,l3);
			for ( int i=0; i<result.size(); i++)
			{
				data << result[i];
				data << "\t";
			}
			data << endl;
			
			l1.clear();
			l2.clear();
			l3.clear();
			result.clear();
			steps++;
		} 
		file.close();
		data.close();
	}
	return 0;
}

//gph : (nlogn) bcppp plus rapide que n² (pr gph voir video tp)
//avec 250 mots, tri fusion et tri rapide quasi lineaire, tri par insertion plus rapide pour ptt nbre de mot (n faible)
